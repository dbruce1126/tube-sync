extern crate getopts;
extern crate leveldb;

use std::old_io::TempDir;
use leveldb::database::Database;
use leveldb::iterator::Iterable;
use leveldb::options::{Options,WriteOptions,ReadOptions};

//getopts help message
fn print_usage(program: &str, _opts: &[getopts::OptGroup]) {
    println!("Usage: {} [options]", program);
    //println!("-s\t\tSeed"); put args here
    println!("-h --help\tUsage");
}

fn main() {
    /* start handling opts */
    let args: Vec<String> = std::os::args();

    let program = args[0].clone();

    let opts = &[
    getopts::reqopt("s", "seed", "set mnemonic seed", ""),
    getopts::optflag("h", "help", "print this help menu")
    ];
    let matches = match getopts::getopts(args.tail(), opts) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };
    if matches.opt_present("h") {
        print_usage(program.as_slice(), opts);
        return;
    }
    // let seed = match matches.opt_str("s") { //example of a req arg
    //     Some(x) => x,
    //     None => panic!("No seed given"),
    // };
    let tempdir = TempDir::new("demo").unwrap();
    let path = tempdir.path().join("simple");

    let mut options = Options::new();
    options.create_if_missing = true;
    let mut database = match Database::open(path, options) {
        Ok(db) => { db },
        Err(e) => { panic!("failed to open database: {}", e) }
    };

    let write_opts = WriteOptions::new();
    match database.put(write_opts, 1, &[1]) {
        Ok(_) => { () },
        Err(e) => { panic!("failed to write to database: {}", e) }
    };

    let read_opts = ReadOptions::new();
    let res = database.get(read_opts,1);
    match res {
        Ok(data) => {
            assert!(data.is_some());
            assert_eq!(data, Some(vec![1]));
        }
        Err(e) => { panic!("failed reading data: {}", e) }
    }

    let read_opts = ReadOptions::new();
    let mut iter = database.iter(read_opts);
    let entry = iter.next();
    assert_eq!( entry, Some((1, vec![1])) );
}
