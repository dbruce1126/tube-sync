# tube-sync
A cli application to download your latest unwatched YouTube videos from subscribed channels


## Prerequisites

`snappy` and `leveldb` need to be installed. On Ubuntu, I recommend.

```sh
sudo apt-get install libleveldb-dev libsnappy-dev
```
